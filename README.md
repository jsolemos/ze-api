[![Node.js CI](https://github.com/jandersonss/ze-api/workflows/Node.js%20CI/badge.svg)](https://github.com/jandersonss/ze-api/actions) <space><space> 
[![pipeline status](https://gitlab.com/jsolemos/ze-api/badges/master/pipeline.svg)](https://gitlab.com/jsolemos/ze-api/-/commits/master) <space><space> [![coverage report](https://gitlab.com/jsolemos/ze-api/badges/master/coverage.svg)](https://gitlab.com/jsolemos/ze-api/-/commits/master)

# Zé Delivery | partner-api


## Repositories
- original - https://gitlab.com/jsolemos/ze-api/
    - pipeline (test | build | deploy) -> https://gitlab.com/jsolemos/ze-api/-/pipelines
- mirror - https://github.com/jandersonss/ze-api
    - pipeline (test) -> https://github.com/jandersonss/ze-api/actions


### Requirements for development

- node 12
- docker
- docker-compose

# Development

## Config

it is necessary to configure the environment variables

    MONGO_URL=mongodb://localhost/ze-partners # example
    PORT=3000 # default

## Debug with Vscode

Create the `.env` file with the necessary environment variables at the root of the project or copy and correct the data in` example.env` and use the `Launch Program` option in the Vscode debugger



## Running

    $ npm run dev

## Running with docker

    $ docker-compose up -d
    $ curl http://localhost:3000/ #health

## Running unit tests

    $ npm run test


## Running integration tests

    $ npm run test:integration

note: it is necessary to have an instance of mongodb running locally

## Running unit + integration tests

    $ npm run test:all

note: it is necessary to have an instance of mongodb running locally

# Build to staging/production

    $ npm run build

## Running

    $ npm run start


## Simple documentation

https://documenter.getpostman.com/view/3196887/Szzn6wap?version=latest

## Postman collection

https://www.getpostman.com/collections/7f1ce4a1fb322ba7932e


## Stating URL

https://ze.omv.jandersonlemos.com/
