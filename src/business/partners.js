export default (dependencies) => {
  const { logger, boom } = dependencies
  const { partner: Partner } = dependencies.models
  return {
    create,
    getById,
    find
  }

  async function create (data) {
    logger.info(data, 'partner > create > params')
    try {
      const partner = await Partner.create({
        ...data,
        document: data.document.replace(/\D/g, '')
      })
      logger.info(partner, 'partner > create > success')
      return partner
    } catch (error) {
      logger.error({
        message: error.message,
        stack: error.stack
      }, 'partner > create > exception')
      mongoErrorHandler(error)
      throw error
    }
  }

  async function getById (id) {
    try {
      logger.info({ id }, 'partner > getById > params')
      const partner = await Partner.findOne({
        _id: id
      })
      if (!partner) {
        throw boom.notFound('No partner found')
      }
      logger.info(partner, 'partner > getById > success')
      return partner
    } catch (error) {
      logger.error({
        message: error.message,
        stack: error.stack
      }, 'partner > getById > exception')
      mongoErrorHandler(error)
      throw error
    }
  }

  async function find ({ lat, lng }) {
    try {
      logger.info({ lat, lng }, 'partner > find > params')
      const partner = await Partner.findOne({
        coverageArea: {
          $geoIntersects: {
            $geometry: {
              type: 'Point',
              coordinates: [parseFloat(lat), parseFloat(lng)]
            }
          }
        }
      })
      if (!partner) {
        throw boom.notFound('No partner found')
      }
      logger.info(partner, 'partner > find > success')
      return partner
    } catch (error) {
      logger.error({
        message: error.message,
        stack: error.stack
      }, 'partner > find > exception')
      mongoErrorHandler(error)
      throw error
    }
  }

  function mongoErrorHandler (err) {
    if (err.errors) {
      throw boom.badRequest(err._message || err.message, err.errors)
    } else if (err.code === 11000) {
      throw boom.badRequest('Partner already exists')
    } else if (err.code === 16755) {
      throw boom.badRequest(
        'There are errors in the coordinates entered, please review and try again.',
        { error: err.message }
      )
    }
  }
}
