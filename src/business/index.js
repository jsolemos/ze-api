import partners from './partners'
export default (dependencies) => {
  return {
    partners: partners(dependencies)
  }
}
