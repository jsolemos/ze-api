import boom from '@hapi/boom'
import get from 'lodash/get'
import partner from './partners'
const partnerMock = {
  id: 1,
  tradingName: 'Adega da Cerveja - Pinheiros',
  ownerName: 'Zé da Silva',
  document: '1432132123891/0001', // CNPJ
  coverageArea: {
    type: 'MultiPolygon',
    coordinates: [
      [[[30, 20], [45, 40], [10, 40], [30, 20]]],
      [[[15, 5], [40, 10], [10, 20], [5, 10], [15, 5]]]
    ]
  }, // Área de Cobertura
  address: {
    type: 'Point',
    coordinates: [-46.57421, -21.785741]
  } // Localização do PDV
}

const dependencies = {
  boom,
  logger: {
    info () { },
    error () { }
  },
  models: {
    partner: {
      findOne (params) {
        if (get(params, 'coverageArea.$geoIntersects.$geometry.coordinates[0]') === 0) {
          return null
        }
        return partnerMock
      },
      create (data) {
        if (!data.tradingName) {
          // eslint-disable-next-line no-throw-literal
          throw {
            message: 'partner validation failed',
            errors: [{
              ownerName: {
                message: 'Path `ownerName` is required.'
              }
            }]
          }
        }
        return partnerMock
      }
    }
  }
}

describe('Partner Business', () => {
  const business = partner(dependencies)
  it('create', async () => {
    const result = await business.create(partnerMock)
    expect(result).toBe(partnerMock)
  })

  it('create:error', async () => {
    const result = await business.create({ ...partnerMock, tradingName: null }).catch(err => err)
    expect(result.message).toBe('partner validation failed')
    expect(result.isBoom).toBe(true)
  })

  it('getById', async () => {
    const result = await business.getById(1)
    expect(result).toBe(partnerMock)
  })

  it('find', async () => {
    const result = await business.find({
      lat: 1,
      lng: 2
    })
    expect(result).toBe(partnerMock)
  })
  it('find:notfound', async () => {
    const result = await business.find({
      lat: 0,
      lng: 0
    }).catch(err => err)
    expect(result.message).toBe('No partner found')
  })

  it('find:genericError', async () => {
    dependencies.models.partner.findOne = async () => {
      throw new Error('error')
    }
    const result = await business.find({
      lat: 1,
      lng: 1
    }).catch(err => err)
    expect(result.message).toBe('error')
  })
})
