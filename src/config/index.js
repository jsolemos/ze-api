const { PORT, MONGO_URL } = process.env

export default {
  server: {
    port: PORT || 3000
  },
  db: {
    url: MONGO_URL || 'mongodb://'
  }
}
