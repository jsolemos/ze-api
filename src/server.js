import app, { logger, mongoConnect } from './app'
import config from './config'

mongoConnect()

app.listen(config.server.port, () => {
  logger.info({}, 'server running')
})

process.on('unhandledRejection', (err) => {
  logger.error(
    {
      ...err,
      message: err.message,
      stack: err.stack
    },
    'unhandledRejection'
  )
})

process.on('uncaughtException', (err) => {
  logger.error(
    {
      ...err,
      message: err.message,
      stack: err.stack
    },
    'uncaughtException'
  )
})
