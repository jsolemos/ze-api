import boom from '@hapi/boom'
import Joi from '@hapi/joi'
import express from 'express'
import httpContext from 'express-http-context'
import expressLogger from 'express-pino-logger'
import helmet from 'helmet'
import isNil from 'lodash/isNil'
import mongoose from 'mongoose'
import autoIncrement from 'mongoose-auto-increment'
import pino from 'pino'
import { v4 } from 'uuid'
import Business from './business'
import config from './config'
import Controllers from './controllers'
import Models from './models'
import Routes from './routes'
import ResponseHandler from './utils/responseHandler'
import Router from './utils/router'
import SchemaValidator from './utils/schemaValidator'

const logger = pino({
  name: 'ze-api',
  level: process.env.LOG_LEVEL || 'info',
  mixin () {
    return {
      requestId: httpContext.get('request-id')
    }
  }
})

autoIncrement.initialize(mongoose.connection)
mongoose.Promise = global.Promise

const mongoConnect = async () => {
  return await mongoose.connect(config.db.url, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  }).catch(async (err) => {
    logger.error({
      url: config.db.url,
      message: err.message,
      stack: err.stack
    }, 'mongo connection error')
    await new Promise(resolve => setTimeout(resolve, 300))
    process.exit(1)
  })
}

const app = express()
const expressRouter = express.Router()
const responseHandler = ResponseHandler({ logger })
const schemaValidator = SchemaValidator({ boom })
const router = Router({ expressRouter, logger, boom, responseHandler })
const models = Models({ mongoose, autoIncrement })
const business = Business({ logger, boom, models })
const controllers = Controllers({ logger, business })
const routes = Routes({ expressRouter: router, Joi, boom, controllers, logger, schemaValidator })

app.use(helmet())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(httpContext.middleware)

app.use((req, res, next) => {
  const id = req.headers['request-id'] || v4()
  httpContext.set('request-id', id)
  req.headers['request-id'] = id
  next()
})

app.use(expressLogger({ logger }))

app.use(routes)

function formatBoomPayload (error) {
  return {
    ...error.output.payload,
    ...(isNil(error.data) ? {} : { data: error.data })
  }
}

app.use(function (err, req, res, next) {
  logger.error({
    message: err.message,
    stack: err.stack
  }, 'handlerServerError')
  if (err.isBoom) {
    sendResponseError(err)
  } else {
    const error = boom.boomify(err, { statusCode: 500 })
    sendResponseError(error)
  }
  function sendResponseError (err) {
    res.status(err.output.statusCode).send(
      formatBoomPayload(err)
    )
  }
})
export {
  app as default,
  logger,
  config,
  mongoConnect,
  mongoose
}
