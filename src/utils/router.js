export default ({ expressRouter, responseHandler, boom }) => {
  return {
    originalRouter: null,
    routes: {},
    setRoute (method, path, ...handlers) {
      this.routes[`${method}:::${path}`] = {
        method,
        handlers
      }
    },
    get (path, ...handlers) {
      this.setRoute('get', path, ...handlers)
    },
    post (path, ...handlers) {
      this.setRoute('post', path, ...handlers)
    },
    put (path, ...handlers) {
      this.setRoute('put', path, ...handlers)
    },
    delete (path, ...handlers) {
      this.setRoute('delete', path, ...handlers)
    },
    res (res) {
      return {
        isSend: false,
        statusCode: 200,
        status (code) {
          this.statusCode = code
          return this
        },
        send (data) {
          this.isSend = true
          res.status(this.statusCode).send(data)
        },
        json (data) {
          this.isSend = true
          res.status(this.statusCode).json(data)
        }
      }
    },
    apply () {
      if (this.originalRouter) return this.originalRouter
      for (const [key, route] of Object.entries(this.routes)) {
        const [method, path] = key.split(':::')
        const routerHandler = route.handlers.pop()
        const routerMiddlewares = route.handlers
        expressRouter[method](
          path,
          ...routerMiddlewares,
          async (req, res, next) => {
            try {
              const reply = this.res(res)
              const response = await routerHandler(req, reply, next)
              checkResponseError(response)
              if (!reply.isSend) {
                res
                  .status(
                    String(response.code).length === 3 ? response.code : 200
                  )
                  .json(responseHandler.success(response))
              }
            } catch (error) {
              next(error)
            }
          }
        )
      }
      this.originalRouter = expressRouter
      return expressRouter
    }
  }

  function checkResponseError (response) {
    if (typeof response !== 'object') return false
    const isBoom = response.isBoom
    if (isBoom) {
      throw response
    }

    const isResponseHandler =
      (String(response.code).length === 3 &&
        !!response.code &&
        String(response.code)[0] !== '2' &&
        !!response.statusCode &&
        String(response.statusCode)[0] !== '2') ||
      !!response.error

    if (isResponseHandler) {
      throw new boom.Boom(
        typeof response.data === 'string' ? response.data : response.message,
        {
          statusCode: response.statusCode || response.code,
          data: response.data
        }
      )
    }
  }
}
