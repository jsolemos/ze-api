export default ({ logger }) => {
  return {
    success: (data, statusCode = 200, message = 'success') => {
      logger.debug('responseHandler:success', data)
      return {
        statusCode,
        message,
        data
      }
    },
    error: (data, statusCode = 400, message = 'error') => {
      logger.debug('responseHandler:error', data)
      return {
        statusCode,
        message,
        data
      }
    }
  }
}
