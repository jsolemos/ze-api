export default ({ boom }) =>
  function validator (fullSchema) {
    return async (req, res, next) => {
      try {
        await Promise.all(
          ['body', 'params', 'query'].map(async (key) => {
            const schema = fullSchema[key]
            if (schema) {
              await schema.validateAsync(req[key], {
                abortEarly: false,
                allowUnknown: true
              })
            }
          })
        )
        next()
      } catch (error) {
        next(boom.badRequest('requestValidation', error.details))
      }
    }
  }
