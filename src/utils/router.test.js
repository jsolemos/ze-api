import assert from 'assert'
import { Router } from 'express'
import routing from './router'

const expressRouter = Router()
const depedencies = {
  expressRouter: expressRouter,
  responseHandler: {
    success: (data, statusCode = 200, message = 'success') => {
      return {
        statusCode,
        message,
        data
      }
    }
  }
}
const res = {
  data: null,
  status () {
    return this
  },
  json (data) {
    this.data = data
  },
  send (data) {
    return this.json(data)
  }
}

const router = routing(depedencies)

describe('Router', () => {
  it('get', () => {
    const path = '/get'
    const method = 'get'
    router[method](path, async () => ({ message: 'success' }))
    assert.strictEqual(!!router.routes[`${method}:::${path}`], true)
  })

  it('put', () => {
    const path = '/put'
    const method = 'put'
    router[method](path, async () => {
      throw new Error('Error')
    })
    assert.strictEqual(!!router.routes[`${method}:::${path}`], true)
  })

  it('post', () => {
    const path = '/post'
    const method = 'post'
    router[method](path, async (req, res) => {
      res.send({ message: 'success' })
    })
    assert.strictEqual(!!router.routes[`${method}:::${path}`], true)
  })

  it('post', () => {
    const path = '/post'
    const method = 'get'
    router[method](path, async (req, res) => {
      res.send({ message: 'success' })
    })
    assert.strictEqual(!!router.routes[`${method}:::${path}`], true)
  })

  it('delete', () => {
    const path = '/delete'
    const method = 'delete'
    router[method](path, async () => ({ message: 'success' }))
    assert.strictEqual(!!router.routes[`${method}:::${path}`], true)
  })

  it('apply', () => {
    const eRoute = router.apply()
    const allRoutes = Object.entries(router.routes).every(([key, route]) => {
      const [method, path] = key.split(':::')
      return eRoute.stack.some(
        (item) =>
          item.route.path === path &&
          item.route.methods[route.method] === true &&
          route.method === method
      )
    })
    assert.strictEqual(allRoutes, true)
  })

  it('handler', async () => {
    const eRoute = router.apply()
    await eRoute.stack[0].route.stack[0].handle({}, res, () => {})
    assert.strictEqual(res.data.message, 'success')
  })

  it('handler:calling:res', async () => {
    res.data = null
    await router.originalRouter.stack
      .find((item) => item.route.path === '/post')
      .route.stack[0].handle({}, res, () => {})
    assert.strictEqual(res.data.message, 'success')
  })

  it('handler:error', async () => {
    let error
    res.data = null
    await router.originalRouter.stack
      .find((item) => item.route.path === '/put')
      .route.stack[0].handle({}, res, (err) => {
        error = err
      })
    assert.strictEqual(error.message, 'Error')
  })
})
