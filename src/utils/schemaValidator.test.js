import boom from '@hapi/boom'
import Joi from '@hapi/joi'
import SchemaValidator from './schemaValidator'

describe('SchemaValidator', () => {
  const schema = {
    body: Joi.object({
      title: Joi.string().required()
    }),
    query: Joi.object({
      id: Joi.number().required()
    })
  }
  const validator = SchemaValidator({ boom })(schema)

  it('invalid schema', async () => {
    try {
      await validator(
        {
          body: {
            title: null
          },
          query: {
            id: 1
          },
          params: {
            id: 1
          }
        },
        {},
        (err) => {
          if (err) {
            throw err
          }
        }
      )
    } catch (error) {
      expect(error.isBoom).toBe(true)
      expect(error.message).toBe('requestValidation')
      expect(error.data[0].message).toBe('"title" must be a string')
    }
  })
  it('valid schema', async () => {
    try {
      await validator(
        {
          body: {
            title: 'teste'
          },
          query: {
            id: 1
          }
        },
        {},
        (err) => {
          if (err) {
            throw err
          }
        }
      )
    } catch (error) {
      expect(error).not.toBeTruthy()
    }
  })
})
