import ResponseHandler from './responseHandler'

const dependencies = {
  logger: {
    debug () {}
  }
}
describe('ResponseHandler', () => {
  const responseHandler = ResponseHandler(dependencies)
  it('success', async () => {
    const data = responseHandler.success({
      status: 'ok'
    })
    expect(data).toHaveProperty('statusCode')
    expect(data).toHaveProperty('data')
    expect(data).toHaveProperty('message')
    expect(data.data).toHaveProperty('status')
    expect(data.data.status).toBe('ok')
  })

  it('error', async () => {
    const data = responseHandler.error({
      status: 'error'
    })
    expect(data).toHaveProperty('statusCode')
    expect(data).toHaveProperty('data')
    expect(data).toHaveProperty('message')
    expect(data.data).toHaveProperty('status')
    expect(data.data.status).toBe('error')
  })
})
