import boom from '@hapi/boom'
import Joi from '@hapi/joi'
import partner from './partners'

const dependencies = {
  boom,
  Joi,
  expressRouter: {
    count: 0,
    get () {
      this.count++
    },
    post () {
      this.count++
    }
  },
  controllers: {
    partner: {
      create () {},
      getById () {},
      find () {}
    }
  },
  schemaValidator () {
    return (req, res, next) => {}
  }
}
test('Partner init Routes', () => {
  dependencies.expressRouter.count = 0
  partner(dependencies)
  expect(dependencies.expressRouter.count).toBe(3)
})

test('Partner init Routes with error', () => {
  expect.assertions(1)
  dependencies.expressRouter.post = () => {
    throw new Error('Error')
  }
  try {
    partner(dependencies)
  } catch (error) {
    expect(error.message).toBe('Error')
  }
})
