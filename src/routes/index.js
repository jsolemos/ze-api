import partner from './partners'

export default ({ expressRouter, controllers, Joi, schemaValidator }) => {
  partner({ expressRouter, controllers, schemaValidator, Joi })
  expressRouter.get('/', (req, res) => ({
    status: 'running'
  }))
  return expressRouter.apply()
}
