export default ({ Joi }) => {
  const query = Joi.object({
    lat: Joi.number().required(),
    lng: Joi.number().required()
  })

  return {
    query
  }
}
