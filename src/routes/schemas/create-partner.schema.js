export default ({ Joi }) => {
  const partner = Joi.object({
    tradingName: Joi.string().min(3).required(),
    ownerName: Joi.string().min(3).required(),
    document: Joi.string().required(),
    coverageArea: Joi.object({
      type: Joi.string().valid('MultiPolygon').required(),
      coordinates: Joi.array()
        .items(
          Joi.array().items(
            Joi.array().items(Joi.array().items(
              Joi.number().required()
            )).required()
          ).required()
        )
        .required()
    }).required(),
    address: Joi.object({
      type: Joi.string().valid('Point').required(),
      coordinates: Joi.array().items(
        Joi.number().required()
      ).required()
    }).required()
  })

  return {
    body: partner
  }
}
