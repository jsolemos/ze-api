export default ({ Joi }) => {
  const params = Joi.object({
    id: Joi.number()
  })

  return {
    params
  }
}
