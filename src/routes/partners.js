import createPartnerSchema from './schemas/create-partner.schema'
import findPartnerSchema from './schemas/find-partner.schema'
import getPartnerSchema from './schemas/get-partner.schema'
export default ({ expressRouter, controllers, schemaValidator, Joi }) => {
  const { partner } = controllers

  expressRouter.post(
    '/partners',
    schemaValidator(createPartnerSchema({ Joi })),
    partner.create
  )
  expressRouter.get(
    '/partners',
    schemaValidator(findPartnerSchema({ Joi })),
    partner.find
  )
  expressRouter.get(
    '/partners/:id',
    schemaValidator(getPartnerSchema({ Joi })),
    partner.getById
  )
}
