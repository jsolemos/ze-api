export default ({ mongoose, autoIncrement }) => {
  const MultiPolygonSchema = new mongoose.Schema({
    type: {
      type: String,
      required: [true]
    },
    coordinates: [[[[Number]]]]
  })

  const PointSchema = new mongoose.Schema({
    type: {
      type: String,
      required: [true]
    },
    coordinates: [Number]
  })

  const PartnerSchema = new mongoose.Schema({
    tradingName: {
      type: String,
      required: [true]
    },
    ownerName: {
      type: String,
      required: [true]
    },
    document: {
      type: String,
      required: [true],
      unique: true
    },
    coverageArea: {
      type: MultiPolygonSchema,
      index: '2dsphere'
    },
    address: {
      type: PointSchema,
      index: '2dsphere'
    }
  })

  PartnerSchema.method('toJSON', function () {
    var obj = this.toObject()
    // Rename fields
    obj.id = obj._id
    delete obj._id
    delete obj.__v
    delete obj.coverageArea.__v
    delete obj.coverageArea._id
    delete obj.address.__v
    delete obj.address._id
    return obj
  })
  PartnerSchema.plugin(autoIncrement.plugin, {
    model: 'Partner',
    startAt: 1
  })
  const Partner = mongoose.model('partner', PartnerSchema)
  return Partner
}
