import mongoose from 'mongoose'
import autoIncrement from 'mongoose-auto-increment'
import PartnerModel from './partner'

autoIncrement.initialize(mongoose.connection)

const partnerMock = {
  id: 1,
  tradingName: 'Adega da Cerveja - Pinheiros',
  ownerName: 'Zé da Silva',
  document: '1432132123891/0001', // CNPJ
  coverageArea: {
    type: 'MultiPolygon',
    coordinates: [
      [
        [
          [30, 20],
          [45, 40],
          [10, 40],
          [30, 20]
        ]
      ],
      [
        [
          [15, 5],
          [40, 10],
          [10, 20],
          [5, 10],
          [15, 5]
        ]
      ]
    ]
  }, // Área de Cobertura
  address: {
    type: 'Point',
    coordinates: [-46.57421, -21.785741]
  } // Localização do PDV
}

const dependencies = {
  mongoose,
  autoIncrement
}
describe('Partner Model', () => {
  const Partner = PartnerModel(dependencies)
  it('checkModel', async () => {
    expect(Partner).toHaveProperty('create')
    expect(Partner).toHaveProperty('findById')
    expect(Partner).toHaveProperty('findOne')
  })
  it('toJSON', async () => {
    const model = new Partner(partnerMock).toJSON()
    expect(model).toHaveProperty('id')
    expect(model).not.toHaveProperty('_id')
    expect(model).not.toHaveProperty('__v')
    expect(model).not.toHaveProperty('converageArea._id')
    expect(model).not.toHaveProperty('converageArea.__v')
    expect(model).not.toHaveProperty('address._id')
    expect(model).not.toHaveProperty('address.__v')
  })
})
