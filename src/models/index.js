import partner from './partner'
export default ({ mongoose, autoIncrement }) => ({
  partner: partner({ mongoose, autoIncrement })
})
