import partner from './partners'

export default (dependencies) => {
  return {
    partner: partner(dependencies)
  }
}
