export default ({ business }) => {
  const { partners } = business
  return {
    create,
    getById,
    find
  }

  async function create (req, _res) {
    const data = await partners.create(req.body)
    return data
  }
  async function getById (req, _res) {
    const data = await partners.getById(req.params.id)
    return data
  }
  async function find (req, _res) {
    const data = await partners.find(req.query)
    return data
  }
}
