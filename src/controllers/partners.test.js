import partner from './partners'

const dependencies = {
  business: {
    partners: {
      async create (partner) {
        return partner
      },
      async getById (id) {
        return {
          id
        }
      },
      async find () {
        return [{}]
      }
    }
  }
}

describe('Partner Controller', () => {
  const controller = partner(dependencies)
  it('create', async () => {
    const result = await controller.create({
      body: {
        id: 1
      }
    })
    expect(result).toBeTruthy()
  })

  it('getById', async () => {
    const result = await controller.getById({
      params: {
        id: 1
      }
    })
    expect(result).toBeTruthy()
  })
  it('find', async () => {
    const result = await controller.find({
      query: {
        lat: 1,
        lng: 1
      }
    })
    expect(result).toBeTruthy()
    expect(result).toBeInstanceOf(Array)
  })
})
