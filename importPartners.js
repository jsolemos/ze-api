const axios = require('axios')
const _ = require('lodash')

const run = async () => {
  const { data: { pdvs } } = await axios.get('https://raw.githubusercontent.com/ZXVentures/ze-code-challenges/master/files/pdvs.json')
  for (const partnerData of pdvs) {
    try {
      const { data: partner } = await axios.post('http://ze.omv.jandersonlemos.com/partners', partnerData)
      console.log('=>', partner)
    } catch (error) {
      console.error('create partner error', _.get(error, 'response.data'), JSON.stringify(partnerData))
    }
  }
}

run().catch(err =>
  console.log(err))
