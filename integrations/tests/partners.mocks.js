export const partnerMock = {
  tradingName: 'Mercado Pinheiros',
  ownerName: 'Luiz Santo',
  document: '06004905000116',
  coverageArea: {
    coordinates: [
      [
        [
          [
            -46.623238,
            -21.785538
          ],
          [
            -46.607616,
            -21.819803
          ],
          [
            -46.56676,
            -21.864737
          ],
          [
            -46.555088,
            -21.859322
          ],
          [
            -46.552685,
            -21.848167
          ],
          [
            -46.546677,
            -21.836536
          ],
          [
            -46.51801,
            -21.832712
          ],
          [
            -46.511143,
            -21.821877
          ],
          [
            -46.489857,
            -21.81805
          ],
          [
            -46.480587,
            -21.810083
          ],
          [
            -46.503418,
            -21.797491
          ],
          [
            -46.510284,
            -21.793667
          ],
          [
            -46.518696,
            -21.794304
          ],
          [
            -46.52831,
            -21.785538
          ],
          [
            -46.56882,
            -21.767365
          ],
          [
            -46.600235,
            -21.77119
          ],
          [
            -46.619118,
            -21.768799
          ],
          [
            -46.627872,
            -21.7739
          ],
          [
            -46.628044,
            -21.782349
          ],
          [
            -46.623238,
            -21.785538
          ]
        ]
      ]
    ],
    type: 'MultiPolygon'
  },
  address: {
    coordinates: [
      -46.57421,
      -21.785742
    ],
    type: 'Point'
  },
  id: 63
}
