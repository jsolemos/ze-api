//
import request from 'supertest'
import app, { config, mongoConnect, mongoose } from '../../src/app'
import { partnerMock } from './partners.mocks'

describe('/partner', () => {
  beforeAll(() => {
    // force replace config connection
    config.db.url = process.env.CI_BUILD_REF_NAME ? 'mongodb://mongo/test' : 'mongodb://localhost/test'
    mongoConnect()
  })
  afterAll(async () => {
    try {
      await new Promise(resolve => setTimeout(resolve, 1000))
      await mongoose.connection.dropDatabase()
      await mongoose.connection.close()
    } catch (error) {
      console.error(error)
    }
  })

  describe('POST /', () => {
    it('should create and return new partner', async () => {
      const { status, body } = await request(app)
        .post('/partners')
        .send(partnerMock)

      expect(status).toBe(200)
      expect(body).toBeTruthy()
      expect(body).toHaveProperty('statusCode')
      expect(body.statusCode).toBe(200)
      expect(body).toHaveProperty('data')
      expect(body.data).toHaveProperty('id')
      expect(body.data.tradingName).toBe(partnerMock.tradingName)
    })

    it('should return an error because partner already exists', async () => {
      const { status, body } = await request(app)
        .post('/partners')
        .send(partnerMock)

      expect(status).toBe(400)
      expect(body).toBeTruthy()
      expect(body).toHaveProperty('statusCode')
      expect(body.statusCode).toBe(400)
      expect(body.message).toBe('Partner already exists')
    })

    it('should return an error because there is no field coverageArea', async () => {
      const { status, body } = await request(app)
        .post('/partners')
        .send({ ...partnerMock, coverageArea: undefined })

      expect(status).toBe(400)
      expect(body).toBeTruthy()
      expect(body).toHaveProperty('statusCode')
      expect(status).toBe(400)
      expect(body.message).toBe('requestValidation')
      expect(body.data.length).toBe(1)
    })

    it('should return an error because there is no field coverageArea.coordinates', async () => {
      const { status, body } = await request(app)
        .post('/partners')
        .send({
          ...partnerMock,
          coverageArea: {
            ...partnerMock.coverageArea,
            coordinates: []
          }
        })

      expect(status).toBe(400)
      expect(body).toBeTruthy()
      expect(body).toHaveProperty('statusCode')
      expect(status).toBe(400)
      expect(body.message).toBe('requestValidation')
      expect(body.data.length).toBe(1)
    })

    it('should return an error because there is no field address', async () => {
      const { status, body } = await request(app)
        .post('/partners')
        .send({
          ...partnerMock,
          address: undefined
        })

      expect(status).toBe(400)
      expect(body).toBeTruthy()
      expect(body).toHaveProperty('statusCode')
      expect(status).toBe(400)
      expect(body.message).toBe('requestValidation')
      expect(body.data.length).toBe(1)
    })

    it('should return an error because there is field coverageArea.type != MultiPolygon and invalid coordinates ', async () => {
      const { status, body } = await request(app)
        .post('/partners')
        .send({
          ...partnerMock,
          coverageArea: {
            type: 'A',
            coordinates: [1, 2]
          }
        })

      expect(status).toBe(400)
      expect(body).toBeTruthy()
      expect(body).toHaveProperty('statusCode')
      expect(status).toBe(400)
      expect(body.message).toBe('requestValidation')
      expect(body.data.length).toBe(4)
    })

    it('should return an error because there is field address.type != Point', async () => {
      const { status, body } = await request(app)
        .post('/partners')
        .send({
          ...partnerMock,
          address: {
            type: 'A',
            coordinates: [1, 2]
          }
        })

      expect(status).toBe(400)
      expect(body).toBeTruthy()
      expect(body).toHaveProperty('statusCode')
      expect(status).toBe(400)
      expect(body.message).toBe('requestValidation')
      expect(body.data.length).toBe(1)
    })

    it('should return an error because there is no field address.coordinates', async () => {
      const { status, body } = await request(app)
        .post('/partners')
        .send({
          ...partnerMock,
          address: {
            ...partnerMock.address,
            coordinates: []
          }
        })

      expect(status).toBe(400)
      expect(body).toBeTruthy()
      expect(body).toHaveProperty('statusCode')
      expect(status).toBe(400)
      expect(body.message).toBe('requestValidation')
      expect(body.data.length).toBe(1)
    })

    it('should return an error because there is no field tradingName , ownerName and document', async () => {
      const { status, body } = await request(app)
        .post('/partners')
        .send({
          ...partnerMock,
          ownerName: null,
          tradingName: null,
          document: null
        })

      expect(status).toBe(400)
      expect(body).toBeTruthy()
      expect(body).toHaveProperty('statusCode')
      expect(status).toBe(400)
      expect(body.message).toBe('requestValidation')
      expect(body.data.length).toBe(3)
    })
  })

  describe('GET /', () => {
    describe('/', () => {
      it('should return a partner near', async () => {
        const { status, body } = await request(app)
          .get('/partners')
          .query({
            lat: partnerMock.coverageArea.coordinates[0][0][0][0],
            lng: partnerMock.coverageArea.coordinates[0][0][0][1]
          })
        expect(status).toBe(200)
        expect(body.data.id).toBe(1)
        expect(body.data.tradingName).toBe(partnerMock.tradingName)
      })

      it('should return an error because a near partner was not found', async () => {
        const { status, body } = await request(app)
          .get('/partners')
          .query({
            lat: partnerMock.coverageArea.coordinates[0][0][0][0] + 1,
            lng: partnerMock.coverageArea.coordinates[0][0][0][1] + 2
          })
        expect(status).toBe(404)
        expect(body.message).toBe('No partner found')
      })

      it('should return an error because params lat and lng not exists', async () => {
        const { status, body } = await request(app)
          .get('/partners')
        expect(status).toBe(400)
        expect(body.message).toBe('requestValidation')
        expect(body.data.length).toBe(2)
      })
    })

    describe('/:id', () => {
      it(`should return partner with id 1 (${partnerMock.tradingName})`, async () => {
        const { status, body } = await request(app)
          .get('/partners/1')

        expect(status).toBe(200)
        expect(body.data.id).toBe(1)
        expect(body.data.tradingName).toBe(partnerMock.tradingName)
      })

      it('should return an error because partner with id 2 not exists', async () => {
        const { status, body } = await request(app)
          .get('/partners/2')

        expect(status).toBe(404)
        expect(body.message).toBe('No partner found')
      })

      it('should return an error param id is not number', async () => {
        const { status, body } = await request(app)
          .get('/partners/a')

        expect(status).toBe(400)
        expect(body.message).toBe('requestValidation')
      })
    })
  })
})
