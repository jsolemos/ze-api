// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

module.exports = {
  collectCoverageFrom: ['src/**/*.js'],
  // The directory where Jest should output its coverage files
  coverageDirectory: './coverage',

  // An array of regexp pattern strings used to skip coverage collection
  coveragePathIgnorePatterns: [
    '/node_modules/',
    'integrations/',
    'src/config/index.js',
    'src/app.js',
    'src/server.js',
    'src/controllers/index.js',
    'src/business/index.js',
    'src/routes/index.js',
    'src/models/index.js'
  ],

  // A list of reporter names that Jest uses when writing coverage reports
  coverageReporters: [
    // "json",
    'text',
    'lcov'
    //   "clover"
  ],

  // An object that configures minimum threshold enforcement for coverage results
  coverageThreshold: {
    global: {
      branches: 60,
      functions: 80,
      lines: 80
      // "statements": -10
    }
  },
  testEnvironment: 'node',

  testPathIgnorePatterns: [
    'dist',
    'integrations'
  ],
  transform: {
    '.(js|jsx|ts|tsx)': '@sucrase/jest-plugin'
  },

  verbose: true

}
