FROM node:12.13.0-alpine

WORKDIR /app

COPY . .

RUN npm ci --production && npm run build

EXPOSE 3000

CMD ["npm", "run", "start"]